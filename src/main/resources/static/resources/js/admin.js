app.controller('adminController', function($scope, $http, $rootScope) {
    
	$scope.votos;
	$scope.obras;
	$scope.search;
	
	$scope.init = function() {
		$http.get('getVotos').then(function(response) {
			if(response.status == 200) {
				$scope.votos = response.data;
				
				var obras = new Map();
				_.each($scope.votos, function(voto) {
					if(obras.get(voto.obra.id) == undefined) {
						obras.set(voto.obra.id, { obra: voto.obra, votos: [] });
					}
					var votoC = { id: voto.id, data: voto.data, email: voto.email };
					obras.get(voto.obra.id).votos.push(votoC);
				});
				
				$scope.obras = Array.from(obras.values());
				
				$scope.obras.sort(function(a, b) {
					return b.votos.length - a.votos.length;
				});
			}
		});
		$(".datepicker").datepicker();
	};
	
	$scope.exportarCSVTable = function(idTable, filename) {
		var rows = [];
		
		$(idTable + " thead tr").each(function(i, v) {
			var row = [];
			$(this).children("th").each(function(j, z) {
				row.push($(this).text());
			});
			rows.push(row);
		});
		
		$(idTable + " tbody tr").each(function(i, v) {
			var row = [];
			$(this).children("td").each(function(j, z) {
				row.push($(this).text());
			});
			rows.push(row);
		});
		
		$scope.exportarCSV(filename, rows);
	};
	
	$scope.exportarCSV = function (filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            
            for (var j = 0; j < row.length; j++) {
            	
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                } else if(typeof row[j] == "boolean") {
                	innerValue = row[j] ? "S" : "N";
                }
                
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                
                if (j > 0)
                    finalVal += ';';
                
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob(["\ufeff", csvFile], { type: 'text/csv;charset=utf-8;' });
        
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
            
        } else {
            var link = document.createElement("a");
            
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    };

	$scope.init();
	
});
