var app = angular.module('app', ['ngSanitize', 'ui.select']).run(function($rootScope, $http, alert) {
	$rootScope.token = $("meta[name='_csrf']").attr("content");
	$rootScope.header = $("meta[name='_csrf_header']").attr("content");
	$rootScope.config = { headers:{} };
	$rootScope.config.headers[$rootScope.header] = $rootScope.token;
	
	$rootScope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
    	$('.collapse').on('show.bs.collapse', function () {
    	    $('.collapse.in').collapse('hide');
    	});
	});
	
	$rootScope.formatDate = function(date) {
    	return date ? moment(date).format('DD/MM/YYYY') : null;
    };
    
    $rootScope.formatDateHour = function(date) {
    	return date ? moment(date).format('DD/MM/YYYY HH:mm:ss') : null;
    };
    
    $rootScope.formatMoeda = function(num) {
    	return numeral(num).format('0,0.00');
    };
});

app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

app.factory('alert', function() {
    return {
        success: function(msg) {
        	var arg = { title: "Mensagem", color: "#739E73", icon: "fa fa-check"};
    		
    		$.bigBox({
    			title : arg.title,
    			content : msg,
    			color : arg.color,
    			icon : arg.icon,
    			timeout : 5000
    		});
        },
        error: function(response) {
        	var arg = { title: "Erro!", color: "#C46A69", icon: "fa fa-warning animated"};
    		
    		$.bigBox({
    			title : arg.title,
    			content : response.config.url + ": " + response.status + " - " + response.statusText,
    			color : arg.color,
    			icon : arg.icon,
    			timeout : 5000
    		});
        }
    };
});

app.factory('i18n', function($http) {
    return {
    	initLang: function($scope, loc) {
			$scope.lang = { locale: "pt_br", nome: "Português", sigla: "PT", classe: "flag-br" };
			if(loc == "en_us" || loc == "en_US") {
				$scope.lang = { locale: "en_us", nome: "English", sigla: "US", classe: "flag-us" };
			}
		},
		
		setLocale: function($scope, i18n, loc, reload) {
			$http.get("locale/" + loc).then(function (response) {
				i18n.initLang(response.data);
		    	if(reload) window.location = "";
		    }, function error(response) {
		    	alert.error(response);
		    });
		}
    };
});

app.factory('utilService', function($http, $rootScope) {
	return {

		broadcastChilds : function(childs, args) {
			if (typeof childs != 'undefined'
					&& childs.replace(/\s/gm, '') != '') {
				var arr = childs.replace(/\s/gm, '').split(',');
				for (var c = 0; c < arr.length; c++) {
					if (arr[c] && typeof arr[c] != 'undefined')
						$rootScope.$broadcast(arr[c], args);
				}
			}
		}
	
	};
});

app.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			var keys = Object.keys(props);

			items.forEach(function(item) {
				var itemMatches = false;

				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					if (prop) {
						if (prop.indexOf(".") != -1) {
							var text = props[prop] ? props[prop].toString().toLowerCase() : "";
							var arrProp = prop.split(".");
							var itemProp = arrProp && item[arrProp[0]] ? eval("item['" + arrProp.join("']['") + "']"): "";
							if (itemProp && itemProp.toString().toLowerCase().indexOf(text) !== -1) {
								itemMatches = true;
								break;
							}
						} else {
							var text = props[prop] ? props[prop].toLowerCase() : "";
							if (item[prop] && item[prop].toString().toLowerCase().indexOf(text) !== -1) {
								itemMatches = true;
								break;
							}
						}
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});