app.controller('mainController', function($scope, $http) {
    
	$scope.obras;
	$scope.obra;
	
	$scope.votar = function(id) {
		window.location = "cadastro?id=" + id;
	};
	
	$scope.init = function() {
		$http.get('getObras').then(function(response) {
			if(response.status == 200) {
				$scope.obras = response.data;
			}
		});
		
		$scope.id = $.urlParam('id');
		
		if($scope.id) {
			$http.get('getObraById', { params : { obraId: $scope.id } }).then(function(response) {
				if(response.status == 200) {
					$scope.obra = response.data;
				}
			});
		}
	};
	
	$scope.init();
	
});
