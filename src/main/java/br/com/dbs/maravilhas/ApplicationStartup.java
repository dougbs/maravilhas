package br.com.dbs.maravilhas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.dbs.maravilhas.model.Obra;
import br.com.dbs.maravilhas.repository.ObraRepository;
import br.com.dbs.maravilhas.repository.VotoRepository;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private VotoRepository votoRepository;

	@Autowired
	private ObraRepository obraRepository;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {

		votoRepository.deleteAll();
		obraRepository.deleteAll();

		Obra obra1 = new Obra("Basílica N. S. da Penha", "resources/img/obras/basilica.jpeg", "Caio Holanda Garcia", "Caio Blass");
		Obra obra2 = new Obra("Centro Cultural da Penha", "resources/img/obras/centro_cultural.jpeg", "Alessandro Dairel", "Fedos");
		Obra obra3 = new Obra("Clube Esportivo da Penha", "resources/img/obras/clube.jpeg", "Jonathan S. Silva", "Axé");
		Obra obra4 = new Obra("Igreja do Rosário", "resources/img/obras/igreja.jpeg", "Thiago Falgetano", "Falge");
		Obra obra5 = new Obra("Igreja Matriz N.S. da Penha", "resources/img/obras/igreja_matriz.jpeg", "Rodrigo Lopes Lima", "Motim");
		Obra obra6 = new Obra("Memorial Penha de França", "resources/img/obras/memorial.jpeg", "Vandre Luiz de Oliveira", "Don");
		Obra obra7 = new Obra("Mercado Municipal da Penha", "resources/img/obras/mercado.jpeg", "Danilo R. S. Santos", "Briga");
		Obra obra8 = new Obra("Parque Tiquatira", "resources/img/obras/parque.jpeg", "Rodrigo Pereira de Melo", "Digue");
		Obra obra9 = new Obra("Shopping Penha", "resources/img/obras/shopping.jpeg", "Alessandra A. Siqueira Gomes", "Alessandra Siqueira");
		Obra obra10 = new Obra("Vinícola Lucano", "resources/img/obras/vinicola.jpeg", "Lucas Bizzar", "Bizzar");

		obraRepository.save(obra1);
		obraRepository.save(obra2);
		obraRepository.save(obra3);
		obraRepository.save(obra4);
		obraRepository.save(obra5);
		obraRepository.save(obra6);
		obraRepository.save(obra7);
		obraRepository.save(obra8);
		obraRepository.save(obra9);
		obraRepository.save(obra10);

		return;
	}

}