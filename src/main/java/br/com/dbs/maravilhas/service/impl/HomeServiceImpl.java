package br.com.dbs.maravilhas.service.impl;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.dbs.maravilhas.model.Obra;
import br.com.dbs.maravilhas.model.Voto;
import br.com.dbs.maravilhas.repository.ObraRepository;
import br.com.dbs.maravilhas.repository.VotoRepository;
import br.com.dbs.maravilhas.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {

	@Autowired
	private ObraRepository obraRepository;

	@Autowired
	private VotoRepository votoRepository;

	private static final Logger logger = LoggerFactory.getLogger(HomeServiceImpl.class);

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public List<Obra> getObras() {
		return obraRepository.findAll();
	}

	@Override
	public Obra getObraById(Long obraId) {
		return obraRepository.findOne(obraId);
	}

	@Override
	public void votar(Long obraId, String nome, String email) {
		String ip = null;
		try {
			ip = NetworkInterface.getNetworkInterfaces().nextElement().getInetAddresses().nextElement().getHostAddress();
		} catch (SocketException e) {
			logger.error("Erro ao recuperar IP", e);
		}
		Obra obra = obraRepository.findOne(obraId);
		Date data = new Date();
		Voto voto = new Voto(nome, email, ip, data, dateFormat.format(data), obra);
		votoRepository.save(voto);
	}

	@Override
	public Boolean votouHoje(String email) {
		List<Voto> votosHoje = votoRepository.findByEmailAndDia(email, dateFormat.format(new Date()));
		return !votosHoje.isEmpty();
	}

	@Override
	public List<Voto> getVotos() {
		return votoRepository.findAll(new Sort(Direction.DESC, "data"));
	}

}
