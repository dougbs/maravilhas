package br.com.dbs.maravilhas.service;

import br.com.dbs.maravilhas.model.User;

public interface UserService {

	void save(User user);

	User findByEmail(String email);

	void update(User user);

	Boolean verifyPass(String email, String token);

	long count();

}
