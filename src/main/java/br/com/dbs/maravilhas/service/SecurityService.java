package br.com.dbs.maravilhas.service;

public interface SecurityService {

	String findLoggedInUsername();

	void autologin(String email, String password);

}
