package br.com.dbs.maravilhas.service.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbs.maravilhas.model.User;
import br.com.dbs.maravilhas.repository.UserRepository;
import br.com.dbs.maravilhas.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());

	@Autowired
	private UserRepository userRepository;

	@Override
	public void save(User user) {
		userRepository.save(user);
	}

	@Override
	public void update(User user) {
		userRepository.save(user);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Boolean verifyPass(String email, String token) {

		if (email != null && token != null) {

			User dbUser = userRepository.findByEmailAndPassword(email, token);

			if (dbUser != null && token != null) {
				return true;
			}

		}

		return false;
	}

	@Override
	public long count() {
		return userRepository.count();
	}

}
