package br.com.dbs.maravilhas.service;

import java.util.List;

import br.com.dbs.maravilhas.model.Obra;
import br.com.dbs.maravilhas.model.Voto;

public interface HomeService {

	List<Obra> getObras();

	Obra getObraById(Long obraId);

	void votar(Long obraId, String nome, String email);

	Boolean votouHoje(String email);

	List<Voto> getVotos();

}
