package br.com.dbs.maravilhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dbs.maravilhas.model.Obra;

public interface ObraRepository extends JpaRepository<Obra, Long> {

}
