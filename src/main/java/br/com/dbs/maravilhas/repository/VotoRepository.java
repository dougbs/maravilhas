package br.com.dbs.maravilhas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dbs.maravilhas.model.Voto;

public interface VotoRepository extends JpaRepository<Voto, Long> {

	List<Voto> findByEmailAndDia(String email, String dia);

}
