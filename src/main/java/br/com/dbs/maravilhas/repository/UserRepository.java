package br.com.dbs.maravilhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dbs.maravilhas.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
	
	User findByEmailAndPassword(String email, String password);

}
