package br.com.dbs.maravilhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.dbs.maravilhas.model.Obra;
import br.com.dbs.maravilhas.service.HomeService;

@Controller
public class HomeController {

	@Autowired
	private HomeService homeService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {
		return new ModelAndView("index");
	}

	@RequestMapping(value = "/regras", method = RequestMethod.GET)
	public ModelAndView regras() {
		return new ModelAndView("regras");
	}

	@RequestMapping(value = "/obras", method = RequestMethod.GET)
	public ModelAndView obras() {
		return new ModelAndView("obras");
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public ModelAndView cadastro(@RequestParam(required = false) Long id) {
		return new ModelAndView("cadastro");
	}

	@RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
	public ModelAndView cadastrar(@RequestParam Long obraId, @RequestParam String nome, @RequestParam String email) {
		Boolean votouHoje = homeService.votouHoje(email);

		if (votouHoje) {
			return new ModelAndView("javotou");
		} else {
			homeService.votar(obraId, nome, email);
			return new ModelAndView("confirmacao");
		}
	}

	@RequestMapping(value = "/getObras", method = RequestMethod.GET)
	public ResponseEntity<List<Obra>> getObras() {
		List<Obra> obras = homeService.getObras();
		return new ResponseEntity<List<Obra>>(obras, HttpStatus.OK);
	}

	@RequestMapping(value = "/getObraById", method = RequestMethod.GET)
	public ResponseEntity<Obra> getObraById(@RequestParam Long obraId) {
		Obra obra = homeService.getObraById(obraId);
		return new ResponseEntity<Obra>(obra, HttpStatus.OK);
	}

}
