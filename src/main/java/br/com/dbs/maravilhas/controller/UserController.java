package br.com.dbs.maravilhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.dbs.maravilhas.model.Voto;
import br.com.dbs.maravilhas.service.HomeService;

@Controller
public class UserController {

	@Autowired
	private HomeService homeService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
		return "login";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView admin() {
		return new ModelAndView("admin");
	}

	@RequestMapping(value = "/votos", method = RequestMethod.GET)
	public ModelAndView votos() {
		return new ModelAndView("votos");
	}

	@RequestMapping(value = "/votosPorObra", method = RequestMethod.GET)
	public ModelAndView votosPorObra() {
		return new ModelAndView("votosPorObra");
	}

	@RequestMapping(value = "/getVotos", method = RequestMethod.GET)
	public ResponseEntity<List<Voto>> getVotos() {
		List<Voto> votos = homeService.getVotos();
		return new ResponseEntity<List<Voto>>(votos, HttpStatus.OK);
	}

}
